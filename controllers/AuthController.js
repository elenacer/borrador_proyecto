
const io = require('../io');
const crypt = require('../crypt');

const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechueca13ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

var users = require("../usuarios.json");

const requestJson = require('request-json');

function loginV1(req,res){
  console.log("LOGIN /apitechu/login");
  console.log("El email del usario es " + req.body.email);
  console.log("La psw del usuario es " + req.body.password);

  var result = {};
  result.msg = "Usuario no autorizado";
  for (var i=0; i< users.length; i++){
    if (users[i].email == req.body.email && users[i].password == req.body.password){
        logged = true;
        console.log("Usuario encontrado, el id es: " + users[i].id);
        users[i].logged = true;
        io.writeUserDataToFile(users);
        result.msg = "Login correcto";
        result.userId = users[i].id;
        break;
    }
  }
  res.send(result);
}

function loginV2(req,res){
  console.log("LOGIN /apitechu/v2/login");
  console.log("El email del usario es " + req.body.email);
  console.log("La psw del usuario es " + req.body.password);
  var query = "q=" + JSON.stringify({"email":req.body.email});
  console.log("query es " + query);
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(errGet, resMlabGet, bodyGet){
      if (errGet) {
        var response = {
          "msg" : "Internal error."
        }
        res.status(500).send(response);
      } else {
          if (bodyGet.length > 0){
            var user = bodyGet[0];
            console.log("La psw del usuario sigue siendo " + req.body.password);
            var autorizado = crypt.checkPassword(req.body.password, user.password);
            console.log("autorizado " + autorizado);
            if(autorizado){
                var data = {"$set":{"logged":true}};
                httpClient.put("user?" + query + "&" + mLabAPIKey, data,
                  function(errPut, resMlabPut, bodyPut){
                      if (errPut) {
                        var response = {
                          "msg" : "Internal error."
                        }
                        res.status(500).send(response);
                      } else {
                        console.log(bodyPut);
                        var response = {
                          "msg" : "Usuario logado",
                          "id" : user.id
                        }
                        res.send(response);
                      }
                  }
                )
            } else {
              var response = {
                "msg" : "Usuario no autorizado"
              }
              res.status(401).send(response);
            }
          } else {
            var response = {
              "msg" : "Usuario no autorizado"
            }
            res.status(403).send(response);
          }
      }
    }
  )
}



function logoutV1(req,res){
  console.log("LOGOUT  /apitechu/v1/logout/:id");
  console.log("El id del usuario es el " + req.params.id);
  var msg = "Logout incorrecto";
  var result = {};

  for (var i=0; i< users.length; i++){

    if (users[i].id == req.params.id && users[i].logged != undefined && users[i].logged === true){
        console.log("Usuario encontrado");
        delete users[i].logged;
        io.writeUserDataToFile(users);
        msg = "Logout correcto";
        break;
    }
  }
  result.msg = msg;
  res.send(result);
}


function logoutV2(req,res){
  console.log("LOGOUT  /apitechu/v2/logout/:id");
  console.log("El id del usuario es el " + req.params.id);
  var id = Number.parseInt(req.params.id);
  var query = "q=" + JSON.stringify({"id":id});
  console.log("query es " + query);
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(errGet, resMlabGet, bodyGet){
      if (errGet) {
        var response = {
          "msg" : "Internal error."
        }
        res.status(500).send(response);
      } else{
          if (bodyGet.length > 0){
            var user = bodyGet[0];
            if (user.logged) {
              var data = {"$unset":{"logged":""}};
              httpClient.put("user?" + query + "&" + mLabAPIKey, data,
                function(errPut, resMlabPut, bodyPut){
                  if (errPut) {
                    var response = {
                      "msg" : "Internal error."
                    }
                    res.status(500).send(response);
                  } else {
                    var response = {
                      "msg" : "Logout correcto"
                    }
                    res.send(response);
                   }
                  }
                )
            } else{
              var response = {
                "msg" : "Logout incorrecto"
              }
              console.log("usuario no logado");
              res.status(400).send(response);
            }

          } else {
            var response = {
              "msg" : "Logout incorrecto"
            }
            console.log("usuario no encontrado");
            res.status(400).send(response);

          }
      }
    }
  )
}


module.exports.loginV1= loginV1;
module.exports.loginV2= loginV2;
module.exports.logoutV1= logoutV1;
module.exports.logoutV2= logoutV2;
