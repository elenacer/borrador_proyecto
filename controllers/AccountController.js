const requestJson = require('request-json');

const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechueca13ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountsByUserId(req, res){
  console.log("GET /apitechu/v2/accounts/:id");

  var id = Number.parseInt(req.params.id);
  console.log("La id del titular de la cuenta es: " + id);

  var query = "q=" + JSON.stringify({"userId":id});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente http creado");
  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if (err) {
        var response = {
          "msg" : "Error obteneindo cuentas."
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var response = body;
        } else {
          var response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
        }
        res.send(response);
      }
    }
  )
}

module.exports.getAccountsByUserId = getAccountsByUserId;
