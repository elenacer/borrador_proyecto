const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');

const baseMlabURL ="https://api.mlab.com/api/1/databases/apitechueca13ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");
  //res.sendFile('usuarios.json', {root: __dirname});
  var result={};
  var users = require('../usuarios.json'); //así es un array
  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }
  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;
  res.send(result);
}


function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  };
  console.log(newUser);
  var users = require('../usuarios.json');
  users.push(newUser);
  console.log("usuario añadido al array");
  io.writeUserDataToFile(users);
  console.log("proceso de creación de usuario terminado");
}


function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("El id del usario a borrar es " + req.params.id);
  var users = require("../usuarios.json");
  var deleted =false;
  var id;

  for (var i=0; i< users.length; i++){
    id = users[i].id;
    console.log(id);
    if (id == req.params.id){
        //users.splice(i, -1, 1)
        console.log("Usuario encontrado del array " + users[i].first_name);
        users.splice(i,1);
        deleted =true;
        break;
    }
  }


  io.writeUserDataToFile(users);
}

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente creado");
  httpClient.get("user?" + mLabAPIKey,
    function(err, resMlab, body){
      var response = !err ? body : {
        "msg" : "Error obteneindo usuarios."
      }
      res.send(response);
    }
  )
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");
/*
  var  = ideq.params.id;
  console.log("La id del usario a buscar es " + id);
    var query = 'q={"id":' + id + '}';
*/
  var id = Number.parseInt(req.params.id);
  console.log("La id del usario a buscar es " + id);

  var query = "q=" + JSON.stringify({"id":id});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if (err) {
        var response = {
          "msg" : "Error obteneindo usuarios."
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado."
          }
          res.status(404);
        }
        res.send(response);
      }
    }
  )
}

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.id);
  ///OJO eliminar console log del password
  console.log(req.body.password);
  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  }
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente http creado");
  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMlab, body) {
      console.log("usuario creado en mLab");
      res.status(201).send({ "msg" : "Usuario creado correctamente"});
    }
  )
}

function deleteUserV2(req,res){
  console.log("DELETE /apitechu/v2/users/:id");
  console.log("El id del usario a borrar es " + req.params.id);
  var users = [];
  console.log("Users ");
  console.log(users);
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente http creado");
  var id = Number.parseInt(req.params.id);
  var query = "q=" + JSON.stringify({"id":id});
  console.log("query es " + query);
  httpClient.put("user?" + query + "&" + mLabAPIKey, users,
    function(err, resMlab, body){
      if (err) {
        var response = {
          "msg" : "Error obteneindo usuarios."
        }
        res.status(500);
      } else {
        //console.log(resMlab);
        console.log(body);
        if (body.removed == 1) {
          console.log("Usuario eliminado en mLab");
          res.send({ "msg" : "Usuario eliminado correctamente"})
        } else {
          console.log("Usuario no encontrado");
          res.status(404).send({ "msg" : "Usuario no encontrado"})
        }
      }
    }
  )
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.deleteUserV2 = deleteUserV2;
